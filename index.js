const express = require('express');

const app = express();

app.get('/link_1', (req, res) => {
  res.sendFile(__dirname + '/link_1.html')
})
app.get('/link_2', (req, res) => {
  res.sendFile(__dirname + '/link_2.html')
})
app.get('/link_3', (req, res) => {
  res.sendFile(__dirname + '/link_3.html')
})
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index_served.html')
})

app.use('/static', express.static(__dirname + '/files'))

app.listen(process.env.PORT, () => {
  console.log('server started');
})
